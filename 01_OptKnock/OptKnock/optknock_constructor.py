#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: optknock_constructor
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for constructing the optimization problem for the optknock framework (Burgard et al., 2003, 10.1002/bit.10803)
Done by reading the necessary network parameters from csv files during the preprocessing, then constructing optimization problem from smaller blocks such as the constraints associated with the metabolic network, the knockouts etc.
'''

import util.read_metabolic_network_parameters as csv_parameter_reader
import pyomo.environ as pe
import util.illustrative_problem_construction as illustrative_problem_constructor

def construct_optknock(network_name, target_chemical, substrate_name, num_knockouts, network_reduction=True):
    # Function for constructing the optknock framework
    #Inputs:
    # 1) network_name :Name of metabolic network of interest
    # Note that by default, the metabolic network during preprocessing uses network reduction in the form of graph clustering to improve convergence.
    # currently supports:
    # - "illustrative_problem": small example network highlighting the ned for simultaneous process and strain optimization through exploiting the overflow metabolism
    # - "e_coli_core": E.coli core network(DOI: 10.1128/ecosalplus.10.2.1 )
    # - "iML1515": at the time of writing this file, the most extensive E.coli MG1655 metabolic network available (PMID: 29020004)
    # 2) target_chemical: name of target chemical of interest in form of reaction id (e.g., EX_ac_e for acetate in iML1515)
    # 3) substrate: name of substrate (C-source) supplied to the medium. Currently all other C-sources are then set to be 0
    # 4) num_knockouts: the maximum number of knockouts
    # Output:
    # optknock: pyomo Concretemodel: optimization problem maximizing the target chemical yield with an embedded metabolic network level being reformulated to a single-level problem through strong duality

    # differentiate between route for illustrative problem and route for true metabolic network
    if network_name=="illustrative_problem":
        # Output: matrices for the optimization problem
        [S, S_trans, Q, Q_trans, B] = illustrative_problem_constructor.create_matrices()
        # Output: vectors for the optimization problem
        [b, c_bio, c_obj, c_glc, ub, lb, d] = illustrative_problem_constructor.create_vectors()
        # Output: sets for optimization problem concerning the nonzero entries in the matrices
        [row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q,
                  row_list_of_nonzero_columns_B] = illustrative_problem_constructor.create_metabolic_network_set_parameters()
        #Output: sets for optimization problem concerning knockouts
        [set_reactions_not_to_target, set_blocked_reactions,
         set_possible_knockouts] = illustrative_problem_constructor.create_knockout_set_parameters()
        # Output: indices of biomass reaction, the target chemical and the glucose and oxygen reaction
        [idx_glc_backwards, idx_substrate_uptake, idx_o2_backwards, idx_bm_forward, idx_target_chem_forward] = illustrative_problem_constructor.create_indices()
        # Output: important cardinalities for the different sets
        [num_metabolites, num_reactions_ori, num_reactions_irrev,
        num_add_constr] = illustrative_problem_constructor.create_cardinality_of_sets()

        # disable network_reduction as it is not necessary for the illustrative problem
        network_reduction = False


    else:
        # add '/' to network_name to access folder
        network_name = "../Preprocessing/output/"+network_name + "/"
        csv_parameter_reader.print_metabolic_network_information(network_name)
        # Read all matrices from csv files
        # Input: string containing the folder where the csv files are; Output: matrices for the optimization problem
        [S, B, Q, S_trans, Q_trans] = csv_parameter_reader.preprocessing_metabolic_network_matrix_parameters(network_name)
        # Read all vectors from csv files
        # Input: string containing the folder where the csv files are; Output: vectors for the optimization problem
        [b, c_bio, c_obj, c_glc, ub, lb, d] = csv_parameter_reader.preprocessing_metabolic_network_vector_parameters(network_name)
        # Read all sets from csv files
        # Input: string containing the folder where the csv files are; Output: sets for optimization problem concerning the nonzero entries in the matrices
        [row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q,
        row_list_of_nonzero_columns_B] = csv_parameter_reader.preprocessing_metabolic_network_set_parameters(network_name)
        # Read all sets concerning knockouts from csv files
        # Input: string containing the folder where the csv files are; Output: sets for optimization problem concerning knockouts
        [set_reactions_not_to_target, set_blocked_reactions,
        set_possible_knockouts] = csv_parameter_reader.preprocessing_knockout_set_parameters(network_name)
        # Read all important indices from csv files
        # Input: string containing the folder where the csv files are; Output: indices of biomass reaction, the target chemical and the glucose and oxygen reaction
        [idx_bm_forward, idx_glc_backwards, idx_substrate_uptake, idx_o2_backwards,
        idx_target_chem_forward] = csv_parameter_reader.preprocessing_metabolic_network_indices_parameters(network_name, target_chemical, substrate_name)
        # Read the cardinalities of important sets
        # Input: string containing the folder where the csv files are; Output: important cardinalities for the different sets
        [num_metabolites, num_reactions_ori, num_reactions_irrev,
        num_add_constr] = csv_parameter_reader.preprocessing_metabolic_cardinality_parameters(network_name)
        
        if network_reduction:
            # Import sections from preprocessing, denoting linear paths throughout the metabolic network without any branches
            sections = csv_parameter_reader.preprocessing_sections(network_name)

    # change upper bound from glucose uptake to new substrate
    ub[idx_substrate_uptake] =ub[idx_glc_backwards]
    # Catch case that glucose stays substrate, if not set glucose uptake to zero
    if idx_substrate_uptake != idx_glc_backwards:
        ub[idx_glc_backwards]=0

    # 1.3 Create pyomo sets
    # Create model
    optknock_optimization_problem = pe.ConcreteModel()
    # Create set containing all metabolites
    optknock_optimization_problem.set_metabolites = pe.Set(initialize=range(num_metabolites))
    # Create set of all reactions in metabolic networks, contains both reversible and irreversible reactions
    optknock_optimization_problem.set_reactions_ori = pe.Set(initialize=range(num_reactions_ori))
    # Create second set of reactions, this time with all reversible reactions replaced by two irreversible reactions
    optknock_optimization_problem.set_reactions_irrev = pe.Set(initialize=range(num_reactions_irrev))
    # Create set of possible knockouts
    optknock_optimization_problem.set_possible_knockouts = pe.Set(initialize=set_possible_knockouts)
    # Create set containing all additional linear constraints
    # Unless otherwise specified, contains 2 constraints:
    # v[biomass]>=f*v[biomass][Wild type], which imposes that at least a fraction f of the wild type growth rate
    # is achieved at the optimal solution
    # v[ATPM]>= MAINTENANCE, the non growth associated maintenance (MAINTENANCE) must be supplied, else the cell dies
    optknock_optimization_problem.set_add_constr = pe.Set(initialize=range(num_add_constr))

    if network_reduction:
        # Create set containing all sections
        optknock_optimization_problem.set_number_of_sections = pe.Set(initialize=range(len(sections)))
        # Create sets for each section length
        def rule_sections(problem, k):
            return range(len(sections[k]))
        optknock_optimization_problem.set_number_of_reactions_in_sections = pe.Set(
            optknock_optimization_problem.set_number_of_sections, initialize=rule_sections)
        # Link each section to the number of reactions in the section. E.g., for section with index zero containing three reactions, create (0,0),(0,1),(0,2)
        # Allows for easy implementation of linking constraint (see link_within_section)
        def link_between(problem):
            return ((k, v) for k in problem.set_number_of_sections for v in problem.set_number_of_reactions_in_sections[k])
        optknock_optimization_problem.sections = pe.Set(dimen=2, initialize=link_between)
    
    # 1.4 Definitions of optimization variables
    # 1.4.1 Process level variables
    # binary variables for reaction knockout (y=0) or inclusion (y=1)
    optknock_optimization_problem.y = pe.Var(optknock_optimization_problem.set_reactions_ori, within=pe.Binary)
    # 1.4.2 Metabolic network variables
    # 1.4.2.1 primal variables
    # reaction fluxes( problem is formulated so that fluxes are always positive)
    optknock_optimization_problem.v = pe.Var(optknock_optimization_problem.set_reactions_irrev, bounds=(0, 1000))
    # 1.4.2.2 Dual variables
    # dual variables:
    # upper bound
    upper_bound_duals_lambda = 10000
    upper_bound_duals_mu_lb = 10000
    upper_bound_duals_mu_ub = 10000
    upper_bound_duals_add_constr = 10000
    # dual variables for equality constraint S*v=b, split to ensure positive bounds
    optknock_optimization_problem.lambda_1 = pe.Var(optknock_optimization_problem.set_metabolites,
                                      bounds=(-1 * upper_bound_duals_lambda, upper_bound_duals_lambda))
    # optknock_optimization_problem.lambda_2 = pe.Var(optknock_optimization_problem.set_metabolites, bounds=(0, upper_bound_duals_lambda))
    # dual variables for inequalities (upper and lower bounds, additional linear ineq constraints)
    optknock_optimization_problem.mu_ub = pe.Var(optknock_optimization_problem.set_reactions_irrev, bounds=(0, upper_bound_duals_mu_ub))
    optknock_optimization_problem.mu_lb = pe.Var(optknock_optimization_problem.set_reactions_irrev, bounds=(0, upper_bound_duals_mu_lb))
    optknock_optimization_problem.mu_add_constr = pe.Var(optknock_optimization_problem.set_add_constr, bounds=(0, upper_bound_duals_add_constr))


    # 2 Constraints
    # 2.1 Process level constraints:
    # 2.1.1 Equality constraints
    # certain reactions should not be knocked out
    def reaction_not_to_target(problem, reaction_idx):
        return problem.y[reaction_idx] == 1

    optknock_optimization_problem.reaction_not_to_target = pe.Constraint(set_reactions_not_to_target, rule=reaction_not_to_target)

    # Blocked reactions are set to 0
    def blocked_reaction(problem, reaction_idx):
        return problem.y[reaction_idx] == 0

    optknock_optimization_problem.blocked_reaction = pe.Constraint(set_blocked_reactions, rule=blocked_reaction)

    # Limit number of knockouts below maximum allowed number of knockouts
    def limit_number_of_knockouts(problem):
        return sum((1 - problem.y[i]) for i in problem.set_possible_knockouts) == num_knockouts

    optknock_optimization_problem.limit_number_of_knockouts = pe.Constraint(rule=limit_number_of_knockouts)

    if network_reduction:
        # Create link for sections. i and j are called from the tuples created by optknock_optimization_problem.sections
        def link_within_section(problem, i, j):
            return problem.y[sections[i][0]] == problem.y[sections[i][j]]

        optknock_optimization_problem.link_within_section = pe.Constraint(optknock_optimization_problem.sections, rule=link_within_section)

    # 2.1.2 Inequality constraints
    # Currently no inequalities on process level

    # 2.2 Network level constraints
    # 2.2.1 Equality constraints

    # 2.2.1.1 Primal equality constraints
    # Mass balance for all metabolites:
    def mass_balance_metabolites(problem, metabolite_idx):
        return sum(S[metabolite_idx, j] * problem.v[j] for j in row_list_of_nonzero_columns_S[metabolite_idx]) == b[
            metabolite_idx]

    optknock_optimization_problem.mass_balance_metabolites = pe.Constraint(optknock_optimization_problem.set_metabolites,
                                                             rule=mass_balance_metabolites)

    # 2.2.1.2 Dual equality constraints
    def dual_constr(problem, reaction_idx):
        return sum(S_trans[reaction_idx, j] * (problem.lambda_1[j]) for j in
                   row_list_of_nonzero_columns_S_trans[reaction_idx]) + problem.mu_ub[reaction_idx] - problem.mu_lb[
                   reaction_idx] + sum(
            Q_trans[reaction_idx, j] * problem.mu_add_constr[j] for j in problem.set_add_constr) == c_obj[reaction_idx]  # - problem.lambda_2[j]

    optknock_optimization_problem.dual_constr = pe.Constraint(optknock_optimization_problem.set_reactions_irrev,
                                                                    rule=dual_constr)

    # strong duality: primal objective =dual objective
    def strong_duality(problem):
        return sum(c_obj[i] * problem.v[i] for i in problem.set_reactions_irrev) == -sum(
            (B[i, j] * problem.y[j]) * problem.mu_lb[i] * lb[i] for i in problem.set_reactions_irrev for j in
            row_list_of_nonzero_columns_B[i]) + sum(
            (B[i, j] * problem.y[j]) * problem.mu_ub[i] * ub[i] for i in problem.set_reactions_irrev for j in
            row_list_of_nonzero_columns_B[i]) + sum(d[i] * problem.mu_add_constr[i] for i in
                                                    problem.set_add_constr)

    optknock_optimization_problem.strong_duality = pe.Constraint(rule=strong_duality)

    # 2.2.2 Inequality constraints on the network level
    # Lower bounds on v
    def reaction_lower_bound(problem, reaction_idx):
        return lb[reaction_idx] * sum(
            B[reaction_idx, j] * problem.y[j] for j in row_list_of_nonzero_columns_B[reaction_idx]) <= problem.v[
                   reaction_idx]

    optknock_optimization_problem.reaction_lower_bound = pe.Constraint(optknock_optimization_problem.set_reactions_irrev, rule=reaction_lower_bound)

    # Upper bounds bounds on v
    def reaction_upper_bound(problem, reaction_idx):
        return problem.v[reaction_idx] <= ub[reaction_idx] * sum(
            B[reaction_idx, j] * problem.y[j] for j in row_list_of_nonzero_columns_B[reaction_idx])

    optknock_optimization_problem.reaction_upper_bound = pe.Constraint(optknock_optimization_problem.set_reactions_irrev, rule=reaction_upper_bound)

    # Additional linear(!) constraints, defined during the preprocessing
    # Unless otherwise specified, contains 2 constraints:
    # v[biomass]>=f*v[biomass][Wild type], which imposes that at least a fraction f of the wild type growth rate
    # is achieved at the optimal solution
    # v[ATPM]>= MAINTENANCE, the non growth associated maintenance (MAINTENANCE) must be supplied, else the cell dies
    def add_lin_constr(problem, add_constr_idx):
        return sum(Q[add_constr_idx, j] * problem.v[j] for j in row_list_of_nonzero_columns_Q[add_constr_idx]) <= d[
            add_constr_idx]

    optknock_optimization_problem.add_lin_constr = pe.Constraint(optknock_optimization_problem.set_add_constr, rule=add_lin_constr)

    # 3 Objective
    def obj_rule(problem):
        # Maximize space time yield
        # dilution rate is replaced by cell growth rate
        return problem.v[idx_target_chem_forward]

    optknock_optimization_problem.obj = pe.Objective(rule=obj_rule, sense=pe.maximize)

    return optknock_optimization_problem
