#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************


'''
@file: main_optknock
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for running the Optknock framework (Burgard et al., 2003, 10.1002/bit.10803)
'''
import sys
# enable imports beyond top-level
sys.path.append('..')

import OptKnock.optknock_constructor as optimization_problem_constructor
from Postprocessing import postprocessing
import pyomo.environ as pe
import os
# Inputs:
# 1) network_name :Name of metabolic network of interest
# Note that by default, the metabolic network during preprocessing uses network reduction in the form of graph clustering to improve convergence.
# If unwanted, the user needs to comment out the line starting with "import OptKnock.optknock_constructor_with_network_reduction ..." 
# and create the input files using the preprocessing without the network_reduction suffix
# currently supports:
# - "illustrative_problem": small example network highlighting the need for simultaneous process and strain optimization through
#   exploiting the overflow metabolism
# - "e_coli_core": E.coli core network(DOI: 10.1128/ecosalplus.10.2.1 )
# - "iML1515": at the time of writing this file, the most extensive E.coli MG1655 metabolic network available (PMID: 29020004)
# 2) target_chemical: name of target chemical of interest in form of reaction id (e.g., EX_ac_e for acetate in iML1515)
# 3) substrate: name of substrate (C-source) supplied to the medium. Currently all other C-sources are then set to be 0
# 4) num_knockouts: the maximum number of knockouts
# 5) (optional) network_reduction: Note that by default, the metabolic network during preprocessing uses network reduction in the form of graph clustering to improve convergence

# Output:
# optknock: pyomo Concretemodel: optimization problem maximizing the space time yield of the target chemical under
# mass balance constraints for the process quantities, with an embedded metabolic network level being reformulated to a
# single-level problem through strong duality

network_name = "iML1515_reduced"
target_chem = "EX_ac_e"
substrate = "EX_glc__D_e_reverse"

optknock = optimization_problem_constructor.construct_optknock(network_name=network_name,
                                                                   target_chemical=target_chem, substrate_name=substrate,
                                                                    num_knockouts=2)

# check for an output folder. If none exists, create one
if not os.path.exists('output'):
    os.mkdir('output')
    
optknock.write(filename="output/EX_ac_e_MM.lp", io_options={"symbolic_solver_labels": True})
# Solve optimization problem
# SolverFactory('gurobi').solve(model, tee=True)
opt = pe.SolverFactory('gurobi')
#opt.options['presolve'] = 1
opt.options['threads'] = 8
opt.options['IntFeasTol'] = 1e-9
opt.options['FeasibilityTol'] = 1e-8
opt.options['IntegralityFocus'] = 1
#opt.options['MIPFocus'] = 2
# Problem is nonconvex due to kinetics
opt.options['NonConvex'] = 2


# solve the optimization problem 
result_obj = opt.solve(optknock, logfile='output/EX_ac_e_MM_optknock.log', tee=True)
with open('output/EX_ac_e_MM_res.txt', 'w') as output_file:
    optknock.pprint(output_file)
    output_file.close()

# display knockouts and write them to txt file
postprocessing.postprocess_knockouts(optknock, network_name)
