#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: simulknock_construction
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for constructing the optimization problem for the simultaneous strain and process optimization.
Done by reading the necessary network parameters from csv files during the preprocessing, then constructing optimization problem from smaller blocks such as the constraints associated with the metabolic network, the knockouts etc.
NOTE: The variable simulknock_optimization_problem.Substrate_feed is the substrate concentration in the feed in g/L; The feed rate has to be limited by an upper bound artificially, as it will be at the upper bound anyways. For a different substrate concentration in the feed, the upper bound has to be adapted by the user.
'''
import util.read_metabolic_network_parameters as csv_parameter_reader
import pandas as pd
import pyomo.environ as pe
import util.illustrative_problem_construction as illustrative_problem_constructor


def construct_simulknock(network_name, target_chemical, substrate_name, kinetic_type, num_knockouts, network_reduction=True):
    # Function for constructing the simultaneous strain and process optimization problem
    #Inputs:
    # 1) network_name :Name of metabolic network of interest
    # currently supports:
    # - "illustrative_problem": small example network highlighting the ned for simultaneous process and strain optimization through exploiting the overflow metabolism
    # - "e_coli_core": E.coli core network(DOI: 10.1128/ecosalplus.10.2.1 )
    # - "iML1515": at the time of writing this file, the most extensive E.coli MG1655 metabolic network available (PMID: 29020004)
    # 2) target_chemical: name of target chemical of interest in form of reaction id (e.g., EX_ac_e for acetate in iML1515)
    # 3) substrate: name of substrate (C-source) supplied to the medium. Currently all other C-sources are then set to be 0
    # 4) kinetic_type: choice between monod and Michaelis-Menten kinetic
    # 5) num_knockouts: the maximum number of knockouts  
    # 6) (optional) network_reduction: Note that by default, the metabolic network during preprocessing uses network reduction in the form of graph clustering to improve convergence.
    # Output:
    # simulknock: pyomo Concretemodel: optimization problem maximizing the space time yield of the target chemical under mass balance constraints for the process quantities, with an embedded metabolic network level being reformulated to a single-level problem through strong duality

    # differentiate between route for illustrative problem and route for true metabolic network
    if network_name=="illustrative_problem":
        # Output: matrices for the optimization problem
        [S, S_trans, Q, Q_trans, B] = illustrative_problem_constructor.create_matrices()
        # Output: vectors for the optimization problem
        [b, c_bio, c_obj, c_glc, ub, lb, d] = illustrative_problem_constructor.create_vectors()
        # Output: sets for optimization problem concerning the nonzero entries in the matrices
        [row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q,
                  row_list_of_nonzero_columns_B] = illustrative_problem_constructor.create_metabolic_network_set_parameters()
        #Output: sets for optimization problem concerning knockouts
        [set_reactions_not_to_target, set_blocked_reactions,
         set_possible_knockouts] = illustrative_problem_constructor.create_knockout_set_parameters()
        # Output: indices of biomass reaction, the target chemical and the glucose and oxygen reaction
        [idx_glc_backwards, idx_substrate_uptake, idx_o2_backwards, idx_bm_forward, idx_target_chem_forward] = illustrative_problem_constructor.create_indices()
        # Output: important cardinalities for the different sets
        [num_metabolites, num_reactions_ori, num_reactions_irrev,
        num_add_constr] = illustrative_problem_constructor.create_cardinality_of_sets()

        # Introduce molecular weights of 1 g/mol due to metabolic network not representing any real conversion
        M_glc = 1
        M_target_chem = 1
        
        # disable network_reduction as it is not necessary for the illustrative problem
        network_reduction = False


    else:
        # add '/' to network_name to access folder
        network_name = "../Preprocessing/output/"+network_name + "/"
        csv_parameter_reader.print_metabolic_network_information(network_name)
        # Read all matrices from csv files
        # Input: string containing the folder where the csv files are; Output: matrices for the optimization problem
        [S, B, Q, S_trans, Q_trans] = csv_parameter_reader.preprocessing_metabolic_network_matrix_parameters(network_name)

        # Read all vectors from csv files
        # Input: string containing the folder where the csv files are; Output: vectors for the optimization problem
        [b, c_bio, c_obj, c_glc, ub, lb, d] = csv_parameter_reader.preprocessing_metabolic_network_vector_parameters(network_name)
        # Read all sets from csv files
        # Input: string containing the folder where the csv files are; Output: sets for optimization problem concerning the nonzero entries in the matrices
        [row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q,
        row_list_of_nonzero_columns_B] = csv_parameter_reader.preprocessing_metabolic_network_set_parameters(network_name)
        # Read all sets concerning knockouts from csv files
        # Input: string containing the folder where the csv files are; Output: sets for optimization problem concerning knockouts
        [set_reactions_not_to_target, set_blocked_reactions,
        set_possible_knockouts] = csv_parameter_reader.preprocessing_knockout_set_parameters(network_name)
        # Read all important indices from csv files
        # Input: string containing the folder where the csv files are; Output: indices of biomass reaction, the target chemical and the glucose and oxygen reaction
        [idx_bm_forward, idx_glc_backwards, idx_substrate_uptake, idx_o2_backwards,
        idx_target_chem_forward] = csv_parameter_reader.preprocessing_metabolic_network_indices_parameters(network_name,target_chemical,substrate_name)
        # Read the cardinalities of important sets
        # Input: string containing the folder where the csv files are; Output: important cardinalities for the different sets
        [num_metabolites, num_reactions_ori, num_reactions_irrev,
        num_add_constr] = csv_parameter_reader.preprocessing_metabolic_cardinality_parameters(network_name)
        
        if network_reduction:
            # Import sections from preprocessing, denoting linear paths throughout the metabolic network without any branches
            sections = csv_parameter_reader.preprocessing_sections(network_name)    # Import sections from preprocessing, denoting linear paths throughout the metabolic network without any branches


        # Read molecular weight
        # Get table of all molecular weights from Excel file
        DF_M_w = pd.read_excel(io="../Preprocessing/output/Molecular weights/molecular_weights.xlsx", sheet_name="Molecular weights exchange meta",
                            usecols="A:C", index_col=1)
        # Remove 'EX_' and '_reverse' from substrate name
        M_glc = DF_M_w.loc[substrate_name[3:-8], 'Molecular weight[g/mmol]']
        # Remove 'EX_' from target chemical name
        M_target_chem = DF_M_w.loc[target_chemical[3:], 'Molecular weight[g/mmol]']
        

    # change upper bound from glucose uptake to new substrate
    ub[idx_substrate_uptake] =ub[idx_glc_backwards]
    # Catch case that glucose stays substrate, if not set glucose uptake to zero
    if idx_substrate_uptake != idx_glc_backwards:
        ub[idx_glc_backwards]=0
    # Set parameters to correctly switch between the two possible kinetics
    if kinetic_type=="Michaelis-Menten":
        # Source for MM parameters: (Meadows et al., 2010, 10.1016/j.ymben.2009.07.006)
        K_S = 0.53  # mmol/L
        v_glc_max = 10  # mmol/gCDW/h
        v_kin_max = v_glc_max
        c_kin = c_glc
        # Ensure that correct uptake bound is set
        # NOTE: This may cause optimization problem to become infeasible depending on the amount of glucose available during the FBA.
        # Setting f to 0 should solve this issue
        ub[idx_substrate_uptake]=v_glc_max
    if kinetic_type=="monod":
        # Parameters taken from Wick et al. 2008( https://doi.org/10.1046/j.1462-2920.2001.00231.x)
        # Use parameters from continuous reactor
        K_S = 0.044  # g/L
        v_bm_max = 0.73  # 1/h
        v_kin_max = v_bm_max
        c_kin = c_bio


    # 1.3 Create pyomo sets
    # Create model
    simulknock_optimization_problem = pe.ConcreteModel()
    # Create set containing all metabolites
    simulknock_optimization_problem.set_metabolites = pe.Set(initialize=range(num_metabolites))
    # Create set of all reactions in metabolic networks, contains both reversible and irreversible reactions
    simulknock_optimization_problem.set_reactions_ori = pe.Set(initialize=range(num_reactions_ori))
    # Create second set of reactions, this time with all reversible reactions replaced by two irreversible reactions
    simulknock_optimization_problem.set_reactions_irrev = pe.Set(initialize=range(num_reactions_irrev))
    # Create set of possible knockouts
    simulknock_optimization_problem.set_possible_knockouts = pe.Set(initialize=set_possible_knockouts)
    # Create set containing all additional linear constraints
    # Unless otherwise specified, contains 2 constraints:
    # v[biomass]>=f*v[biomass][Wild type], which imposes that at least a fraction f of the wild type growth rate
    # is achieved at the optimal solution
    # v[ATPM]>= MAINTENANCE, the non growth associated maintenance (MAINTENANCE) must be supplied, else the cell dies
    simulknock_optimization_problem.set_add_constr = pe.Set(initialize=range(num_add_constr))

    if network_reduction:
        # Create set containing all sections
        simulknock_optimization_problem.set_number_of_sections = pe.Set(initialize=range(len(sections)))
        # Create sets for each section length
        def rule_sections(problem, k):
            return range(len(sections[k]))
        simulknock_optimization_problem.set_number_of_reactions_in_sections = pe.Set(
            simulknock_optimization_problem.set_number_of_sections, initialize=rule_sections)
        # Link each section to the number of reactions in the section. E.g., for section with index zero containing three reactions, create (0,0),(0,1),(0,2)
        # Allows for easy implementation of linking constraint (see link_within_section)
        def link_between(problem):
            return ((k, v) for k in problem.set_number_of_sections for v in problem.set_number_of_reactions_in_sections[k])
        simulknock_optimization_problem.sections = pe.Set(dimen=2, initialize=link_between)
    # 1.4 Definitions of optimization variables
    # 1.4.1 Process level variables
    # binary variables for reaction knockout (y=0) or inclusion (y=1)
    simulknock_optimization_problem.y = pe.Var(simulknock_optimization_problem.set_reactions_ori, within=pe.Binary)
    # Process level variables
    upper_bound_process_var = 100
    simulknock_optimization_problem.P = pe.Var(bounds=(0, 1000))  # Target chemical concentration in g/L
    simulknock_optimization_problem.X = pe.Var(bounds=(0, upper_bound_process_var))  # biomass concentration in g_(Cell Dry Weight)/L
    simulknock_optimization_problem.Substrate_feed = pe.Var(bounds=(0,10)) # Substrate concentration in feed in g/L; limit feed rate artificially, will always be at upper bound anyways
    if kinetic_type == "Michaelis-Menten":
        simulknock_optimization_problem.Substrate = pe.Var(bounds=(0,
                                                                10))  # Substrate concentration in reactor in g/L; concentration within reactor can't be larger than feed concentration due to continuous flow through reactor
    # Dilution rate is equal to cell growth rate according to mass balance biomass, so will be left out
    # simulknock_optimization_problem.D = pe.Var(bounds=(0,upper_bound_process_var))

    # 1.4.2 Metabolic network variables
    # 1.4.2.1 primal variables
    # reaction fluxes( problem is formulated so that fluxes are always positive)
    simulknock_optimization_problem.v = pe.Var(simulknock_optimization_problem.set_reactions_irrev, bounds=(0, 1000))
    # 1.4.2.2 Dual variables
    # dual variables:
    # upper bound
    upper_bound_duals_lambda = 10000
    upper_bound_duals_mu_lb = 10000
    upper_bound_duals_mu_ub = 10000
    upper_bound_duals_add_constr = 10000
    upper_bound_duals_kinetics = 10000
    # dual variables for equality constraint S*v=b, split to ensure positive bounds
    simulknock_optimization_problem.lambda_1 = pe.Var(simulknock_optimization_problem.set_metabolites,
                                      bounds=(-1 * upper_bound_duals_lambda, upper_bound_duals_lambda))
    # simulknock_optimization_problem.lambda_2 = pe.Var(simulknock_optimization_problem.set_metabolites, bounds=(0, upper_bound_duals_lambda))
    # dual variables for inequalities (upper and lower bounds, additional linear ineq constraints)
    simulknock_optimization_problem.mu_ub = pe.Var(simulknock_optimization_problem.set_reactions_irrev, bounds=(0, upper_bound_duals_mu_ub))
    simulknock_optimization_problem.mu_lb = pe.Var(simulknock_optimization_problem.set_reactions_irrev, bounds=(0, upper_bound_duals_mu_lb))
    simulknock_optimization_problem.mu_add_constr = pe.Var(simulknock_optimization_problem.set_add_constr, bounds=(0, upper_bound_duals_add_constr))
    simulknock_optimization_problem.mu_kinetics = pe.Var(bounds=(0, upper_bound_duals_kinetics))

    # 1.4.3 Reformulation variables
    # REFORMULATION: Introduce additional variable rational_term for rational part of MM kinetic
    # rational must be between 0 and 1 by definition
    if kinetic_type == "Michaelis-Menten":
        simulknock_optimization_problem.rational_term = pe.Var(bounds=(0, 1))
    if kinetic_type == "monod":
        # we need this large number for the upper bound to ensure feasibility in the case that v is close to v_max (divide by almost zero)
        simulknock_optimization_problem.rational_term_substrate = pe.Var(bounds=(0, 100000000000000000000000000000))
    # 2 Constraints
    # 2.1 Process level constraints:
    # 2.1.1 Equality constraints

    # Mass balance biomass in reactor will be left out, instead dilution rate is replaced by cell growth rate
    # def mass_balance_biomass(problem):
    #    return (problem.v[idx_bm_forward]-problem.D)*problem.X == 0
    #
    #
    # simulknock_optimization_problem.mass_balance_biomass = pe.Constraint(rule=mass_balance_biomass)

    # Mass balance substrate for process level
    if kinetic_type == "Michaelis-Menten":
        def mass_balance_substrate(problem):
            # dilution rate is replaced by cell growth rate
            return -problem.v[idx_substrate_uptake] * M_glc * problem.X + problem.v[idx_bm_forward] * (
                    problem.Substrate_feed - problem.Substrate) == 0
    if kinetic_type == "monod":
        def mass_balance_substrate(problem):
            # dilution rate is replaced by cell growth rate
            return -problem.v[idx_substrate_uptake] * M_glc * problem.X + problem.v[idx_bm_forward] * (
                    problem.Substrate_feed - K_S * problem.rational_term_substrate) == 0  # problem.Substrate_feed - problem.Substrate)

        # Probably need to impose limit on substrate, else will cause problems due to organismm eating too muchs
        def limit_substrate(problem):
            return K_S * problem.rational_term_substrate <= problem.Substrate_feed

        simulknock_optimization_problem.limit_substrate = pe.Constraint(rule=limit_substrate)

    simulknock_optimization_problem.mass_balance_substrate = pe.Constraint(rule=mass_balance_substrate)

    # Mass balance target chemical for reactor level
    def mass_balance_target_chem(problem):
        # dilution rate is replaced by cell growth rate
        return problem.v[idx_target_chem_forward] * M_target_chem * problem.X - problem.P * problem.v[
            idx_bm_forward] == 0

    simulknock_optimization_problem.mass_balance_target_chem = pe.Constraint(rule=mass_balance_target_chem)

    # certain reactions should not be knocked out
    def reaction_not_to_target(problem, reaction_idx):
        return problem.y[reaction_idx] == 1

    simulknock_optimization_problem.reaction_not_to_target = pe.Constraint(set_reactions_not_to_target, rule=reaction_not_to_target)

    # Blocked reactions are set to 0
    def blocked_reaction(problem, reaction_idx):
        return problem.y[reaction_idx] == 0

    simulknock_optimization_problem.blocked_reaction = pe.Constraint(set_blocked_reactions, rule=blocked_reaction)

    # Limit number of knockouts below maximum allowed number of knockouts
    def limit_number_of_knockouts(problem):
        return sum((1 - problem.y[i]) for i in problem.set_possible_knockouts) == num_knockouts

    simulknock_optimization_problem.limit_number_of_knockouts = pe.Constraint(rule=limit_number_of_knockouts)

    if network_reduction:
        # Create link for sections. i and j are called from the tuples created by optknock_optimization_problem.sections
        def link_within_section(problem, i, j):
            return problem.y[sections[i][0]] == problem.y[sections[i][j]]

        simulknock_optimization_problem.link_within_section = pe.Constraint(simulknock_optimization_problem.sections, rule=link_within_section)

    # 2.1.2 Inequality constraints
    # Currently no inequalities on process level

    # 2.2 Network level constraints
    # 2.2.1 Equality constraints
    # Michaelis-Menten Kinetics on glc uptake rate
    # REFORMULATION: for now, introduce additional variable MM for rational part of MM kinetic
    if kinetic_type == "Michaelis-Menten":
        def kinetics_MM(problem):
            # MM has the form (Substrate)/(Substrate+K_S*M_glc)
            return problem.v[idx_substrate_uptake] == v_kin_max * problem.rational_term

        simulknock_optimization_problem.kinetics_MM = pe.Constraint(rule=kinetics_MM)

        # Link reformulated term to rational
        def kinetics_reformulation_rational(problem):
            # rational_term = (Substrate)/(Substrate+K_S*M_glc)
            return problem.rational_term * (problem.Substrate + K_S * M_glc) == problem.Substrate

        simulknock_optimization_problem.kinetics_reformulation_rational = pe.Constraint(
            rule=kinetics_reformulation_rational)

    if kinetic_type == "monod":
        #
        def kinetics_substrate_reformulation(problem):
            return problem.rational_term_substrate * (v_kin_max - problem.v[idx_bm_forward]) == problem.v[
                idx_bm_forward]

        simulknock_optimization_problem.kinetics_substrate_reformulation = pe.Constraint(
            rule=kinetics_substrate_reformulation)

    # 2.2.1.1 Primal equality constraints
    # Mass balance for all metabolites:
    def mass_balance_metabolites(problem, metabolite_idx):
        return sum(S[metabolite_idx, j] * problem.v[j] for j in row_list_of_nonzero_columns_S[metabolite_idx]) == b[
            metabolite_idx]

    simulknock_optimization_problem.mass_balance_metabolites = pe.Constraint(simulknock_optimization_problem.set_metabolites,
                                                             rule=mass_balance_metabolites)
    if kinetic_type == "Michaelis-Menten":
        # 2.2.1.2 Dual equality constraints
        def dual_constr(problem, reaction_idx):
            return sum(S_trans[reaction_idx, j] * (problem.lambda_1[j]) for j in
                       row_list_of_nonzero_columns_S_trans[reaction_idx]) + problem.mu_ub[reaction_idx] - problem.mu_lb[
                       reaction_idx] + sum(
                Q_trans[reaction_idx, j] * problem.mu_add_constr[j] for j in
                problem.set_add_constr) + problem.mu_kinetics * \
                   c_kin[reaction_idx] == c_obj[reaction_idx]  # - problem.lambda_2[j]

        simulknock_optimization_problem.dual_constr = pe.Constraint(simulknock_optimization_problem.set_reactions_irrev,
                                                                    rule=dual_constr)

        # strong duality: primal objective =dual objective
        def strong_duality(problem):
            return sum(c_obj[i] * problem.v[i] for i in problem.set_reactions_irrev) == -sum(
                (B[i, j] * problem.y[j]) * problem.mu_lb[i] * lb[i] for i in problem.set_reactions_irrev for j in
                row_list_of_nonzero_columns_B[i]) + sum(
                (B[i, j] * problem.y[j]) * problem.mu_ub[i] * ub[i] for i in problem.set_reactions_irrev for j in
                row_list_of_nonzero_columns_B[i]) + sum(d[i] * problem.mu_add_constr[i] for i in
                                                        problem.set_add_constr) + problem.mu_kinetics * v_kin_max * problem.rational_term

        simulknock_optimization_problem.strong_duality = pe.Constraint(rule=strong_duality)
    if kinetic_type == "monod":
        # 2.2.1.2 Dual equality constraints
        def dual_constr(problem, reaction_idx):
            return sum(S_trans[reaction_idx, j] * (problem.lambda_1[j]) for j in
                       row_list_of_nonzero_columns_S_trans[reaction_idx]) + problem.mu_ub[reaction_idx] - problem.mu_lb[
                       reaction_idx] + sum(
                Q_trans[reaction_idx, j] * problem.mu_add_constr[j] for j in
                problem.set_add_constr) == c_obj[reaction_idx]  # - problem.lambda_2[j]

        simulknock_optimization_problem.dual_constr = pe.Constraint(simulknock_optimization_problem.set_reactions_irrev,
                                                                    rule=dual_constr)

        # strong duality: primal objective =dual objective
        def strong_duality(problem):
            return sum(c_obj[i] * problem.v[i] for i in problem.set_reactions_irrev) == -sum(
                (B[i, j] * problem.y[j]) * problem.mu_lb[i] * lb[i] for i in problem.set_reactions_irrev for j in
                row_list_of_nonzero_columns_B[i]) + sum(
                (B[i, j] * problem.y[j]) * problem.mu_ub[i] * ub[i] for i in problem.set_reactions_irrev for j in
                row_list_of_nonzero_columns_B[i]) + sum(d[i] * problem.mu_add_constr[i] for i in
                                                        problem.set_add_constr)

        simulknock_optimization_problem.strong_duality = pe.Constraint(rule=strong_duality)

    # 2.2.2 Inequality constraints on the network level
    # Lower bounds on v
    def reaction_lower_bound(problem, reaction_idx):
        return lb[reaction_idx] * sum(
            B[reaction_idx, j] * problem.y[j] for j in row_list_of_nonzero_columns_B[reaction_idx]) <= problem.v[
                   reaction_idx]

    simulknock_optimization_problem.reaction_lower_bound = pe.Constraint(simulknock_optimization_problem.set_reactions_irrev, rule=reaction_lower_bound)

    # Upper bounds bounds on v
    def reaction_upper_bound(problem, reaction_idx):
        return problem.v[reaction_idx] <= ub[reaction_idx] * sum(
            B[reaction_idx, j] * problem.y[j] for j in row_list_of_nonzero_columns_B[reaction_idx])

    simulknock_optimization_problem.reaction_upper_bound = pe.Constraint(simulknock_optimization_problem.set_reactions_irrev, rule=reaction_upper_bound)

    # Additional linear(!) constraints, defined during the preprocessing
    # Unless otherwise specified, contains 2 constraints:
    # v[biomass]>=f*v[biomass][Wild type], which imposes that at least a fraction f of the wild type growth rate
    # is achieved at the optimal solution
    # v[ATPM]>= MAINTENANCE, the non growth associated maintenance (MAINTENANCE) must be supplied, else the cell dies

    def add_lin_constr(problem, add_constr_idx):
        return sum(Q[add_constr_idx, j] * problem.v[j] for j in row_list_of_nonzero_columns_Q[add_constr_idx]) <= d[
            add_constr_idx]

    simulknock_optimization_problem.add_lin_constr = pe.Constraint(simulknock_optimization_problem.set_add_constr, rule=add_lin_constr)

    # 3 Objective
    def obj_rule(problem):
        # Maximize space time yield
        # dilution rate is replaced by cell growth rate
        return problem.P * problem.v[idx_bm_forward]

    simulknock_optimization_problem.obj = pe.Objective(rule=obj_rule, sense=pe.maximize)

    return simulknock_optimization_problem
