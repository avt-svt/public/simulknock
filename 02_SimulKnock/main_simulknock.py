#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: main_simulknock
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for running the simultaneous process and strain optimization problem
'''

import sys
# enable imports beyond top-level
sys.path.append('..')

import SimulKnock.simulknock_construction as optimization_problem_constructor
from Postprocessing import postprocessing
import pyomo.environ as pe
import os
# Optimization problem construction
# Inputs:
# 
# 1) network_name :Name of metabolic network of interest
# currently supports:
# - "illustrative_problem": small example network highlighting the need for simultaneous process and strain optimization through
# exploiting the overflow metabolism
# - "e_coli_core": E.coli core network(DOI: 10.1128/ecosalplus.10.2.1 )
# - "iML1515": at the time of writing this file, the most extensive E.coli MG1655 metabolic network available (PMID: 29020004)
# 2) target_chemical: name of target chemical of interest in form of reaction id (e.g., EX_ac_e for acetate in iML1515)
# 3) substrate: name of substrate (C-source) supplied to the medium. Currently all other C-sources are then set to be 0
# 4) kinetic_type: choice between monod and Michaelis-Menten kinetic
# 5) num_knockouts: the maximum number of knockouts
# 6) (optional) network_reduction: Note that by default, the metabolic network during preprocessing uses network reduction in the form of graph clustering to improve convergence

# Output:
# simulknock: pyomo Concretemodel: optimization problem maximizing the space time yield of the target chemical under
# mass balance constraints for the process quantities, with an embedded metabolic network level being reformulated to a
# single-level problem through strong duality

network_name = "iML1515_reduced"
target_chem = "EX_ac_e"
substrate = "EX_glc__D_e_reverse"
simulknock = optimization_problem_constructor.construct_simulknock(network_name=network_name,
                                                                   target_chemical=target_chem, substrate_name=substrate,
                                                                   kinetic_type="Michaelis-Menten", num_knockouts=2)

# check for an output folder. If none exists, create one
if not os.path.exists('output'):
    os.mkdir('output')

simulknock.write(filename="output/EX_ac_e_MM.lp", io_options={"symbolic_solver_labels":True})
# Solve optimization problem
# SolverFactory('gurobi').solve(model, tee=True)
opt = pe.SolverFactory('gurobi')
#opt.options['ms_enable']=1
opt.options['threads']=8
opt.options['presolve'] = 1
#opt.options['threads'] = 14
opt.options['IntFeasTol'] = 1e-9
opt.options['FeasibilityTol'] = 1e-9
#opt.options['Cuts'] = 3
#opt.options['MinRelNodes']=100
#opt.options['MIPGap'] = 0.01
#opt.options['MIPFocus'] = 3
# Problem is nonconvex due to kinetics
opt.options['NonConvex'] = 2


result_obj = opt.solve(simulknock, logfile='output/EX_ac_e_MM_simulknock.log', tee=True)
str='output/EX_ac_e_MM_res.txt'
with open(str, 'w') as output_file:
    simulknock.pprint(output_file)
    output_file.close()

# display knockouts and write them to .txt file
postprocessing.postprocess_knockouts(simulknock, network_name)

# postprocessing of the most important variables
# unnecessary in case of the illustrative problem
if not network_name == "illustrative_problem":
    postprocessing.postprocess_vars(simulknock, network_name, target_chem, substrate)