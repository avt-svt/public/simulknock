#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: postprocessing
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for postprocessing the optimization results.
'''

import sys
#enable imports beyond top-level
sys.path.append('..')

import util.read_metabolic_network_parameters as csv_parameter_reader
import numpy as np
import pandas as pd
import pyomo.environ as pe

def postprocess_knockouts(problem, network_name):
    '''
    display knockouts and write them to a .txt file
    '''

    sol_y=np.array([])
    for i in problem.set_reactions_ori:
        sol_y=np.append(sol_y, pe.value(problem.y[i]))
    # Use of 0.9 to not catch values of y slightly below integrality tolerance, e.g., 0.999999
    reaction_knockout=np.where(sol_y<0.9)[0]
    reaction_knockout=np.intersect1d(reaction_knockout,problem.set_possible_knockouts.data())

    # no need to load reaction names for the illustrative problem
    if network_name == "illustrative_problem":
        print()
        print("Knockouts:\n")
        print(sol_y)
        print()

    # load reaction names and ids
    else:
        DF_reactions=pd.read_csv(f"../Preprocessing/output/{network_name}/reaction_names_and_ids.csv",usecols=[1,2])
        print()
        print("Knockouts:\n")
        print(DF_reactions.iloc[reaction_knockout,:])
        print()

        # write knockouts to file
        with open('output/knockouts.txt', 'w') as output_file:
            output_file.write(str(DF_reactions.iloc[reaction_knockout,:]))
    
    return sol_y

def postprocess_vars(problem, network_name, target_chemical, substrate_name):
    '''
    display the most important variables and write them to a .csv file
    '''

    # Read all important indices from csv files
    # Input: string containing the folder where the csv files are;
    # Output: indices of biomass reaction, the target chemical and the glucose and oxygen reaction
    output_dir = f'../Preprocessing/output/{network_name}/'
    [idx_bm_forward, idx_glc_backwards, idx_substrate_uptake, idx_o2_backwards,
         idx_target_chem_forward] = csv_parameter_reader.preprocessing_metabolic_network_indices_parameters(output_dir,target_chemical,substrate_name)

    # store important variables in dictionary
    names = [
            'Space-time yield',
            'Product concentration',
            'Biomass concentration',
            'Substrate concentration in feed',
            'biomass flux (growth rate)',
            'Substrate uptake rate',
            'Flux target chemical'
            ]

    values = [
            pe.value(problem.obj),
            pe.value(problem.P),
            pe.value(problem.X),
            pe.value(problem.Substrate_feed),
            pe.value(problem.v[idx_bm_forward]),
            pe.value(problem.v[idx_substrate_uptake]),
            pe.value(problem.v[idx_target_chem_forward])
            ]
    
    units = [
            'g/L/h',
            'g/L',
            'g_(Cell Dry Weight)/L',
            'g/L',
            '1/h',
            'mmol/g_(Cell Dry Weight)/h',
            'mmol/g_(Cell Dry Weight)/h'
            ]

    variables = {'name':names,
                'value':values,
                'unit': units
                  }
    
    #export variables
    output = pd.DataFrame.from_dict(variables).transpose()

    output.to_csv("output/important_variables.csv", index=False)

    # display variables
    for i in range(len(names)):
        print(names[i] + ": " + str(values[i]) + ' ' + units[i])
