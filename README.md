# SimulKnock: Simultaneous design of fermentation and microbe

## Description 

This is the source code for the paper **[Simultaneous design of fermentation and microbe](https://arxiv.org/abs/2401.16188)** (preprint) by A. L. Ziegler, A. Manchanda, M.-D. Stumm, Lars M. Blank, and A. Mitsos.

## Framework

The tools of microbial strain optimization and process optimization are combined in our optimization formulation SimulKnock.

![Framework_structure](doc/images/GraphicalAbstract_horizontal.png)

## Data & Usage

This repository contains following folders:

* **01_OptKnock**: OptKnock [1] implementation
* **02_SimulKnock**: SimulKnock model
* **03_Sequential_approach**: sequential strain optimization via OptKnock [1] and subsequent process optimization
* **Preprocessing**: preprocessing files to configure and reduce metabolic network
* **Postprocessing**: postprocessing files to read out results
* **doc**: documentation
* **util**: util functions

### Data

Three metabolic networks can be found under the following paths:

* E. coli core [2]: `Preprocessing/metabolic networks/e_coli_core.xml`
* iEC1349_Crooks [3]: `Preprocessing/metabolic networks/iEC1349_Crooks.xml`
* E. coli iML1515 [4]: `Preprocessing/metabolic networks/iML1515.xml`

The implementation of the illustrative network can be found under `util/illustrative_network_construction.py`.

### Usage

For **running** OptKnock, SimulKnock or the Sequential approach, please install the required environment (see below) and \
navigate to the OptKnock folder, e.g., via `cd 01_OptKnock`, and then execute `main_optknock.py` for OptKnock \
or \
navigate to the SimulKnock folder, e.g., via `cd 02_SimulKnock`, and then execute `main_simulknock.py` for SimulKnock \
or \
navigate to the Sequential approach folder, e.g., via `cd 03_Sequential_approach`, and then execute `main_sequential_approach.py` for the Sequential approach.

### Making predictions for custom metabolic network and custom process

To make knockout predictions and process design predictions for a given metabolic network, substrate, target chemical, and kinetics (Michaelis-Menten or Monod), please follow these steps:
* 1. Install all required packages (see below)
* 2. Insert your metabolic network as xml-file in `Preprocessing/metabolic networks/` and adapt the network name, the substrate, and the target chemical in `Preprocessing/preprocessing.ipynb` 
* 3. Adapt the kinetics fitting to your custom process in `SimulKnock/simulknock_construction_with_network_reduction.py`
* 4. Run `02_SimulKnock/main_simulknock.py` with adapted network name, target chemical, substrate, number of knockouts, and kinetic type. 

-> The predictions by the model will be printed and stored under **output/important_variables.csv**.



References:
* [1]: Burgard, A.P., Pharkya, P., Maranas, C.D., 2003. Optknock: a bilevel programming framework for identifying gene knockout strategies for microbial strain optimization. Biotechnology and bioengineering 84, 647–657. doi:10.1002/bit.10803.
* [2]: Orth, J.D., Fleming, R.M.T., Palsson, B.Ø., 2010. Reconstruction and use of microbial metabolic networks: the core Escherichia coli metabolic model as an educational guide. EcoSal Plus, 4, 1. doi: 10.1128/ecosalplus.10.2.1.
* [3]:  Monk, J.M., Koza, A., Campodonico, M.A., Machado, D., Seoane, J.M., Palsson, B.O., Herrg ̊ard, M.J., Feist, A.M., 2016. Multi-omics quantification of species variation of escherichia coli links molecular features with strain phenotypes. Cell systems 3, 238–251. doi:10.1016/j.cels.2016.08.013.
* [4]: Monk, J.M., Lloyd, C.J., Brunk, E., Mih, N., Sastry, A., King, Z., Takeuchi, R., Nomura, W., Zhang, Z., Mori, H., Feist, A.M., Palsson, B.O., 2017. iml1515, a knowledgebase that computes escherichia coli traits. Nature biotechnology 35, 904–908. doi:10.1038/nbt.3956 



## Required packages

The code is built upon: 

* **[COBRA package](http://opencobra.sourceforge.net/)**
* **[pandas package](https://pandas.pydata.org/)**
* **[Jupyter package](https://jupyter.org)**
* **[Pyomo package](http://www.pyomo.org/)**
* **[OpenPyXL package](https://openpyxl.readthedocs.io/en/latest/index.html)**
* **[GLPK package](https://www.gnu.org/software/glpk/)**

which need to be installed before using our code.

We recommend **setting up a virtual environment** using the provided `requirements.txt` file.

For running the optimization, a MIQCQP solver is required. 
In our code, we use
* **[gurobi](https://www.gurobi.com/)** as a solver. 

## How to cite this work

Please cite **[our paper](https://arxiv.org/abs/2401.16188)** if you use this code:

This paper:

```
@misc{Ziegler2024,
      title={Simultaneous design of fermentation and microbe}, 
      author={Anita L. Ziegler and Ashutosh Manchanda and Marc-Daniel Stumm and Lars M. Blank and Alexander Mitsos},
      year={2024},
      howpublished={arXiv preprint arXiv:2401.16188},
}
```

Please also refer to the corresponding packages, that we use, if appropriate:

COBRA package 

```
@Article{Ebrahim.2013,
 author = {Ebrahim, Ali and Lerman, Joshua A. and Palsson, Bernhard O. and Hyduke, Daniel R.},
 year = {2013},
 title = {COBRApy: COnstraints-Based Reconstruction and Analysis for Python},
 pages = {74},
 volume = {7},
 journal = {BMC systems biology},
 doi = {10.1186/1752-0509-7-74}
}
```


pandas package

```
@InProceedings{ mckinney-proc-scipy-2010,
  author    = { {W}es {M}c{K}inney },
  title     = { {D}ata {S}tructures for {S}tatistical {C}omputing in {P}ython },
  booktitle = { {P}roceedings of the 9th {P}ython in {S}cience {C}onference },
  pages     = { 56 - 61 },
  year      = { 2010 },
  editor    = { {S}t\'efan van der {W}alt and {J}arrod {M}illman },
  doi       = { 10.25080/Majora-92bf1922-00a }
}
```

Jupyter package

```
@conference{Kluyver2016jupyter,
  title = {Jupyter Notebooks -- a publishing format for reproducible computational workflows},
  author = {Thomas Kluyver and Benjamin Ragan-Kelley and Fernando P{\'e}rez and Brian Granger and Matthias Bussonnier and Jonathan Frederic and Kyle Kelley and Jessica Hamrick and Jason Grout and Sylvain Corlay and Paul Ivanov and Dami{\'a}n Avila and Safia Abdalla and Carol Willing},
  booktitle = {Positioning and Power in Academic Publishing: Players, Agents and Agendas},
  editor = {F. Loizides and B. Schmidt},
  organization = {IOS Press},
  pages = {87 - 90},
  year = {2016}
} 
```

Pyomo package

```
@book{bynum2021pyomo, 
  title={Pyomo--optimization modeling in python}, 
  author={Bynum, Michael L. and Hackebeil, Gabriel A. and Hart, William E. and Laird, Carl D. and Nicholson, Bethany L. and Siirola, John D. and Watson, Jean-Paul and Woodruff, David L.}, 
  edition={Third}, 
  volume={67}, 
  year={2021}, 
  publisher={Springer Science \& Business Media} 
} 
```

```
@article{hart2011pyomo, 
  title={Pyomo: modeling and solving mathematical programs in Python}, 
  author={Hart, William E and Watson, Jean-Paul and Woodruff, David L}, 
  journal={Mathematical Programming Computation}, 
  volume={3}, 
  number={3}, 
  pages={219--260}, 
  year={2011}, 
  publisher={Springer} 
} 
```

OpenPyXL package

```
@misc{Gazoni2023, 
title = {openpyxl - A Python library to read/write Excel 2010 xlsx/xlsm files}
author = {Gazoni, Eric and Clark, Charlie}
url = {https://openpyxl.readthedocs.io/en/latest/index.html#}
}

```

GLPK package

```
@misc{GLPK,
title = {GNU Linear Programming Kit}
author = {Free Software Foundation, Inc.}
url = {https://www.gnu.org/software/glpk/}
}

```

gurobi

```
@misc{gurobi,
  author = {{Gurobi Optimization, LLC}},
  title = {{Gurobi Optimizer Reference Manual}},
  year = 2023,
  url = "https://www.gurobi.com"
}
```