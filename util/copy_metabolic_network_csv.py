#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: copy_metabolic_network_csv
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File contains a function that compares the date of the current contents of the destination folder with
the source folder and copies the files if the source folder has newer versions
Uses time, os and shutil
'''
import os
import time
import shutil

def copy_files(src_dir, dst_dir):
    # Function for copying files from the source diretory to the destination directory
    # Inputs:
    # src_dir - Source directory from where to copy the files
    # dst_dir - Destination directory where to copy and, if necessary, overwrite the files


    #Check if source directory exists
    print("Found source directory:"+str(os.path.exists(src_dir)))

    # find all files in the output from preprocessing
    # output from walk first gives src_dir, then all directories, then files within the subdirectories
    for src_dir, dirs, files in os.walk(src_dir):
        for file in files:

            # path to source file
            src_file=os.path.join(src_dir,file)
            # path to destination file
            dst_file = os.path.join(dst_dir,file)
            # check if file exists at destination
            if(os.path.exists(dst_file)):
                # Check if source has newer file
                if(os.path.getmtime(src_file)>os.path.getmtime(dst_file)):
                    shutil.copy(src_file,dst_dir)
            else:
                shutil.copy(src_file, dst_dir)