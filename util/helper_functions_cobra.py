#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: helper_functions_cobra.py
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: 
This file contains functions for interacting with metabolic networks from the cobrapy package. Examples are functions returning all indices containing a certain reaction name.
The contents are used during the transformation of the 
metabolic network from the system biology markup language(SBML) into csv files containing all necessary matrices and vectors to parameterize a simultaneous process & strain optimization
@Notes on documentation:
original network - metabolic network containing both irreversible and reversible reactions
irreversible network - metabolic network where all original network reactions are split into forward and backwards reactions
'''
import numpy as np


def find_variable_index(variables,string):
    ## Function for finding the index of variable with the name 
    # containing string (means more than one hit may be possible)
    #
    # Inputs: 
    # variables - optlang container containing all variables 
    # for both forward and backward reactions
    # string - substring of variable name for which the index is wanted
    #
    # Output:
    # idx[0] - the forward reaction whose name contains string
    # idx[1] - the backwards reaction whose name contains string

    
    idx=np.array([],dtype=int)
    for i in range(len(variables)):
        # If string is contained in the name of the variable, add current index to array
        if string in variables[i].name:
            idx=np.append(idx,i)
    # Check that the string supplied to the function is precise enough, else the wrong index may be used during optimization
    assert len(idx)<3, "The string supplied to find the index of the variable was not precise enough. The following reaction names contain the string "+string+": "+str([variables[i].name for i in idx])
    return idx[0],idx[1]


def find_fixed_zero_flux_reactions(variable_bounds,exclude=np.array([])):
    ## Function to find all indices of irreversible reactions
    # that incorrectly have a backreaction in matrix S,
    # done by analyzing the bounds array for bounds of variables 
    # fixed at 0
    # 
    # Inputs: 
    # variable_bounds - numpy array containing the upper and lower 
    # bounds 
    # of all reactions (of matrix S, which has exactly )
    # Optional exclude - list of indices to exclude
    # (such as exchange reactions,as they might change bounds later on)
    # Output:
    # idx - list of indices where the variable bounds are zero,
    # excluding indices contained in exclude

    # create boolean map for variable bounds fixed at zero
    bool_map=np.all([0,-0] == variable_bounds, axis=-1)
    # then extract the indices where the bounds are 0
    # Note: Slice 0 since result is a tuple
    idx=np.where(bool_map==True)[0]
    
    # Remove entries in the exclude array that are larger than the largest value in idx
    # Should only be included if the pruning functinos at the beginning of the preprocessing are active
    # NOTE: including the next line may cause errors
    # exclude=[exclude[i] for i in range(len(exclude)) if not(exclude[i] > max(idx))]
    
    # find indices that are in list exclude
    idx_exclude=np.searchsorted(idx,exclude)

    # delete entries at position to be excluded
    idx=np.delete(idx,idx_exclude)
    return idx


def get_index_by_react_id(reactions,string):
    ## Function to get indices from all reactions in original network that contain string in the 
    # reaction id
    #
    # Inputs:
    # reactions - cobrapy container containing all reactions
    #  in the original network
    # string - string contained in id of reaction
    #
    # Outputs:
    # idx - numpy array containing indices of all reactions with string in id

    #Create empty array
    idx=np.array([],dtype=int)
    # Iterate through all reactions
    for i in range(len(reactions)):
        # If string is found in id of reaction, append to index list
        if string in reactions[i].id:
            idx=np.append(idx,i)
    return idx


def get_index_no_gpr(reactions):
    ## Function to get all reactions not having a
    #  Gene Protein Reaction Rule (GPR)
    #
    # Inputs:
    # reactions - cobrapy container containing all reactions
    #  in the network
    #
    # Outputs:
    # idx - numpy array containing indices of all reactions 
    # not having a GPR rule and thus not being targetable

    #Create empty array
    idx=np.array([],dtype=int)
    # Iterate through all reactions
    for i in range(len(reactions)):
        # If no GPR exists, append index to indice array
        if len(reactions[i].gpr.genes)==0:
            idx=np.append(idx,i)
    return idx


def get_index_by_react_name(reactions,string):
    ## Function to get indices from all reactions in original network that contain string in the 
    # reaction name
    #
    # Inputs:
    # reactions - cobrapy container containing all reactions
    #  in the original network
    # string - string contained in name of reaction
    #
    # Outputs:
    # idx - numpy array containing indices of all reactions 
    # that have string in their name 

    #Create empty array
    idx=np.array([],dtype=int)
    # Iterate through all reactions
    for i in range(len(reactions)):
        # If string is found in name of reaction, append to index list
        if string in reactions[i].name:
            idx=np.append(idx,i)
    return idx


