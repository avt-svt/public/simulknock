#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: helper_functions_matrices.py
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: 
This file contains the function for constructing the mapping matrix B, the function for finding the nonzero entries in a matrix for each row
and the function for finding the correct index shift
@Notes on documentation:
original network - metabolic network containing both irreversible and reversible reactions
irreversible network - metabolic network where all original network reactions are split into forward and backwards reactions
'''

def create_mapping_matrix(q=2):
    import numpy as np
    # Function to create mapping matrix B
    # Inputs:
    # q - Number of reactions in matrix S
    # Note: as S contains only irreversible reactions,
    #  with reversible reactions being made irreversible 
    # we will have exactly twice the number of reactions of the original network
    # (forward and backward reaction for each reaction in the original network)
    # Thus, the number of columns in the matrix B is q/2
    # Note 2: Deletion of rows corresponding to irreversible reactions
    # having a backreaction will be done at a later point (see find_irrev_reactions)
    # Outputs:
    # B - mapping matrix denoting which irreversible reaction in S is connected to which reaction in the original network
    
    ## Check to make sure that q is an even number
    assert q%2<1, f"the number of rows of B must be an even number, got {q}. Reduction due to removal of reaction occurs later, thus number of rows must be even here"
    # Create identity matrix of for the original reactions
    # Note: int is due to type error otherwise, due to assertion beforehand is safe to use
    I=np.identity(int(q/2))
    # Stack two identity matrixes row by row, creates the following form
    # result=[[1,0,...,0],
    #         [1,0,...,0],
    #         ....
    #         [0,0,...,1],
    #         [0,0,...,1]]
    # Create subarrays of form [[1,0,...,0],
    #                           [1,0,...,0]] 
    B=np.stack((I,I),axis=1)
    # concatenate all subarrays into one large 2D array
    B=np.concatenate(B)
    return B


def find_nonzero_indices_for_matrix(matrix):
    import numpy as np
    #Function that returns list of numpy arrays 
    # containing nonzero indices in matrix
    #
    # Example: [[1,0],[0,1]] will create the output [[0],[1]],
    # as the nonzero index in row 0 is in position 0
    # and the nonzero index in row 1 is in position 1
    #
    # Input:
    # matrix - 2D numpy array that must contain at 
    # least one nonzero entry in all rows
    #
    # Output:
    # idx_in_row - list of numpy arrays, where each numpy array 
    # contains all nonzero entries for row index=list index.

    
    # Find all nonzero entries in matrix
    [nonzero_rows,nonzero_columns]=np.nonzero(matrix)

    # Assert that all rows put into this function have at least 1 nonzero entry
    assert np.all(np.in1d(np.arange(matrix.shape[0]),nonzero_rows)), f"This function only allows arrays where all rows contain at least 1 nonzero value, but the function was called with row {np.array2string(np.where(np.in1d(np.arange(matrix.shape[0]),nonzero_rows)<1)[0])} of the array only containing zeros"

    #Initialize arrays, later on the structure will be 
    # idx_in_row[i]=[idx_in_column]
    idx_in_column=np.array([],dtype=int)
    idx_in_row=[]

    # initialize previous idx as 0
    prev_i=0
    #Iterate through all rows to create subarrays
    for i in range(len(nonzero_rows)):
        #Due to structure of nonzero array, 
        # first fill subarray for each row 
        # Check if row already has an entry
        if nonzero_rows[i]==nonzero_rows[prev_i]:
            idx_in_column=np.append(idx_in_column,nonzero_columns[i])
        # if row has changed, add column subarray to row, 
        else:
            idx_in_row.append(idx_in_column)
            # Reset and begin filling column vector
            idx_in_column=np.array([],dtype=int)
            idx_in_column=np.append(idx_in_column,nonzero_columns[i])
            #update prev_i
            prev_i=i
        # last run through loop does not necessarily enter else statement,
        # caught here
        if i==len(nonzero_rows)-1:
            idx_in_row.append(idx_in_column)
    return idx_in_row


def find_index_shift(array_del_idx,idx_ori):
    import numpy as np
    ## Function that finds the index shift due to the deletion 
    # of the entries in matrices and vectors corresponding to reaction variables fixed to 0
    #
    # Inputs:
    # array_del_idx - numpy array containing all deleted indexes
    # idx_ori - index of interest in the irreversible network
    #
    # Output:
    # count - integer of index shift, to be substracted from index in irreversible network

    #Check how many deletions influenced the index of interest
    count=np.count_nonzero(array_del_idx<idx_ori)
    return count