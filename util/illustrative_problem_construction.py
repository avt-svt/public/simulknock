#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

'''
@file: illustrative_problem_construction
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File responsible for constructing the illustrative problem. Note: the illustrative problem does NOT work together with network reduction. 
'''

import util.helper_functions_matrices as hlp_fnc_matr
import numpy as np

def create_stoichiometric_matrix():
    # Function for creating the stoichiometric matrix of the illustrative problem, will be called in every other function in file to get correct sizes
    # Output:
    # S - stoichiometric matrix of illustrative problem

    # Create matrix:
    S = np.array([[1., -1., -1., 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 1, 0, 1, 0, 1, 0, 1, -1, 0, 0], [0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 1, -1, 0, 0, 1, 0, -1, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, -1],
              [0, 0, 1, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0]])


    # Make it so that certain paths are favored
    S[6, 7] = -0.5
    S[6, 8] = -1
    # Bottom path should overall provide more growth & product,
    # but less efficiently
    S[1, 3] = -1
    S[1, 4] = -2
    return S

def create_indices():
    # Function that sets the indices and returns them
    # need to find indexes for glc, oxygen and biomass
    idx_glc_backwards = 0
    idx_substrate_backwards = 0
    idx_o2_backwards = 2
    idx_bm_forward = 10  # third last entry
    idx_target_chem_forward = 11  # second to last entry
    return idx_glc_backwards, idx_substrate_backwards, idx_o2_backwards, idx_bm_forward, idx_target_chem_forward

def create_matrices():
    # Function that returns the matrices S, S_trans, Q, Q_trans, B
    # Output:
    # S - stoichiometric matrix S
    # S_trans - transpose of S
    # Q - additional constraint matrix Q
    # Q_trans - Transpose of Q

    S = create_stoichiometric_matrix()
    [idx_glc_backwards, idx_substrate_backwards, idx_o2_backwards, idx_bm_forward, idx_target_chem_forward] = create_indices()

    # Create matrix for additional linear constraints
    # Q*v<=d
    # first, create 2D array with dimensions #additional constraints x#vars
    Q = np.zeros((1, S.shape[1]))

    # start with lower bound on biomass
    # v_Biomass>=f*obj_Val_WT => -v_BM<=-f*obj_val_WT
    Q[0, idx_bm_forward] = -1  # signs switched due to negative (see above)

    B = hlp_fnc_matr.create_mapping_matrix(S.shape[1] * 2)
    # As illustrative problem only consists of irreversible reactions, set every second index of b to be associated with a nonoccuring reaction
    idx_nonoccuring_react = np.array([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23])

    # Delete nonoccuring reactions
    B = np.delete(B, idx_nonoccuring_react, axis=0)

    # Create transpose of matrices S and K for dualisation of the primal LP
    S_trans = np.transpose(S)
    Q_trans = np.transpose(Q)

    return S, S_trans, Q, Q_trans, B

def create_vectors():
    # Function that returns the vectors b, c_bio, c_obj, c_glc, ub, lb, d
    # Output:
    # b

    S=create_stoichiometric_matrix()
    [idx_glc_backwards, idx_substrate_backwards, idx_o2_backwards, idx_bm_forward, idx_target_chem_forward] = create_indices()
    # create array for lower bounds and upper bounds
    lb = np.full(S.shape[1], 0)
    ub = np.full(S.shape[1], 1000)

    # change upper bound to 100 instead of 1000
    ub[ub > 100] = 100

    # limit overall glc uptake
    ub[idx_glc_backwards] = 10
    # limit respiratory route by limiting oxygen uptake
    ub[idx_o2_backwards] = 3

    # Create vector for objective function
    # Create vector of zeros with length being number of variables
    c_obj = np.zeros(S.shape[1])
    # Set the biomass reaction to 1
    c_obj[idx_bm_forward] = 1

    # Also create a vector for lower bound on biomass flux / do this
    # so that we can later extend for other, nonstraightforward ineqs.
    # Create vector of zeros with length being number of variables
    c_bio = np.zeros(S.shape[1])
    # Set the biomass reaction to 1
    c_bio[idx_bm_forward] = 1

    # create vector for possible constraint on glucose uptake through
    # Michaelis Menten kinetics
    c_glc = np.zeros(S.shape[1])
    # Set the glc reaction to 1
    c_glc[idx_glc_backwards] = 1

    # Round to two decimal digits
    obj_val_WT = 10
    d = np.zeros(1)
    d[0] = -0.01 * obj_val_WT
    b = np.zeros(S.shape[1])

    return b, c_bio, c_obj, c_glc, ub, lb, d

def create_knockout_set_parameters():
    S=create_stoichiometric_matrix()
    set_reactions_not_to_target = np.array([0, 1, 2, 10, 11, 12])
    set_blocked_reactions=np.array([])
    set_possible_knockouts = np.arange(S.shape[1])

    return set_reactions_not_to_target, set_blocked_reactions, set_possible_knockouts

def create_metabolic_network_set_parameters():
    [S,S_trans, Q,Q_trans,B]= create_matrices()
    row_list_of_nonzero_columns_S = hlp_fnc_matr.find_nonzero_indices_for_matrix(S)
    row_list_of_nonzero_columns_S_trans = hlp_fnc_matr.find_nonzero_indices_for_matrix(S_trans)
    row_list_of_nonzero_columns_Q = hlp_fnc_matr.find_nonzero_indices_for_matrix(Q)
    row_list_of_nonzero_columns_B = hlp_fnc_matr.find_nonzero_indices_for_matrix(B)
    return row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q, row_list_of_nonzero_columns_B

def create_cardinality_of_sets():
    S=create_stoichiometric_matrix()
    num_metabolites = (S.shape[0])
    num_reactions_ori = (S.shape[1])
    num_add_constr = 1
    num_reactions_irrev = S.shape[1]
    return num_metabolites, num_reactions_ori, num_reactions_irrev, num_add_constr






