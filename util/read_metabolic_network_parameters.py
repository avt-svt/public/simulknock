#**********************************************************************************
# Copyright (c) 2023 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/SimulKnock
#
#*********************************************************************************

"""
@file: read_metabolic_network_parameters.py
@author: Marc-Daniel Stumm, Anita Lisa Ziegler, Clemens Kortmann, Ashutosh Manchanda, Alexander Mitsos
@description: File handling the preprocessing of all metabolic network related parameters, such as the stoichiometric matrix, the set of reactions that are not targettable, etc.
The parameters for the metabolic network are read from csv files
NOTE: all functions with prefix "_" are not meant to be called from outside this file
"""
import pandas as pd
import numpy as np


def _read_metabolic_network_matrix_parameters_from_csv(src_dir):
    # Function for reading the metabolic network parameters that are in matrix form from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_S - Dataframe containing the stoichiometric matrix S
    # df_B - Dataframe containing the mapping matrix B
    # df_Q - Dataframe containing the additional constraint matrix Q
    # df_S_trans -  Dataframe containing the stoichiometric matrix transpose S_trans
    # df_Q_trans -  Dataframe containing the additional constraint matrix transpose Q_trans
    # matrices
    df_S = pd.read_csv(src_dir + "S.csv")
    df_B = pd.read_csv(src_dir + "B_matrix.csv")
    df_Q = pd.read_csv(src_dir + "Q.csv")
    df_S_trans = pd.read_csv(src_dir + "S_trans.csv")
    df_Q_trans = pd.read_csv(src_dir + "Q_trans.csv")
    return df_S, df_B, df_Q, df_S_trans, df_Q_trans


def _read_metabolic_network_vector_parameters_from_csv(src_dir):
    # Function for reading the metabolic network parameters that are in vector form from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_b - Dataframe containing right hand side of the equation S*v=b
    # df_c_bio - Dataframe containing a vector with a 1 at the index of the biomass reaction
    # df_c_obj - Dataframe containing a vector with ones at the index of reactions contributing to the lnetwork-level objective
    # df_c_glc - Dataframe containing a vector with a 1 at the index of the glucose uptake reaction
    # df_ub - Dataframe for the upper bounds of the reactions
    # df_lb - Dataframe for the lower bounds of the reactions
    # df_d - Dataframe containing right hand side of the equation Q*v<=d
    # vectors
    df_b = pd.read_csv(src_dir + "b_vector.csv")
    df_c_bio = pd.read_csv(src_dir + "c_bio.csv")
    df_c_obj = pd.read_csv(src_dir + "c_obj.csv")
    df_c_glc = pd.read_csv(src_dir + "c_glc.csv")
    df_ub = pd.read_csv(src_dir + "ub.csv")
    df_lb = pd.read_csv(src_dir + "lb.csv")
    df_d = pd.read_csv(src_dir + "d.csv")
    return df_b, df_c_bio, df_c_obj, df_c_glc, df_ub, df_lb, df_d


def _read_metabolic_network_set_parameters_from_csv(src_dir):
    # Function for reading the metabolic network parameters that are sets concerning nonzero matrix entries from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_set_nonzero_S - Dataframe for set of all nonzero entries in stoichiometric matrix S
    # df_set_nonzero_S_trans - Dataframe for  set of all nonzero entries in stoichiometric matrix transpose S_trans
    # df_set_nonzero_Q - Dataframe for set of all nonzero entries in additional constraint matrix Q
    # df_set_nonzero_B - Dataframe for set of all nonzero entries in mapping matrix B

    # sets
    df_set_nonzero_S = pd.read_csv(src_dir + "set_nonzero_S.csv")
    df_set_nonzero_S_trans = pd.read_csv(src_dir + "set_nonzero_S_trans.csv")
    df_set_nonzero_Q = pd.read_csv(src_dir + "set_nonzero_Q.csv")
    df_set_nonzero_B = pd.read_csv(src_dir + "set_nonzero_B.csv")

    return df_set_nonzero_S, df_set_nonzero_S_trans, df_set_nonzero_Q, df_set_nonzero_B


def _read_knockout_set_parameters_from_csv(src_dir):
    # Function for reading the metabolic network parameters that are sets concerning possible knockouts from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_set_reactions_not_to_target - Dataframe for set of all reactions that are definitely present in solution (y=1)
    # df_set_blocked_reactions - Dataframe for set of all reactions that cannot be reached and are thus fixed to zero(y=0)
    # df_set_possible_knockouts - Dataframe for set of all reactions that can be knocked out

    # sets
    df_set_reactions_not_to_target = pd.read_csv(src_dir + "set_reactions_not_to_target.csv")
    df_set_blocked_reactions = pd.read_csv(src_dir + "set_blocked_reactions.csv")
    df_set_possible_knockouts = pd.read_csv(src_dir + "set_possible_knockouts.csv")
    return df_set_reactions_not_to_target, df_set_blocked_reactions, df_set_possible_knockouts


def _read_metabolic_network_index_parameters_from_csv(src_dir):
    # Function for reading important indices of the reactions from the metabolic network from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_idx_important - Dataframe containing the indices of biomass reaction, the target chemical and the glucose and oxygen reaction TODO: Change preprocessing to more easily switch btween target chemicals by importing all target chemicals(?)

    df_idx_important = pd.read_csv(src_dir + "idx_important.csv")
    df_possible_target_chemicals = pd.read_csv(src_dir + "ex_react.csv", index_col=0, engine='python')
    return df_idx_important, df_possible_target_chemicals


def _read_metabolic_network_cardinality_parameters_from_csv(src_dir):
    # Function for reading the metabolic network parameters that are cardinalities of sets from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_card_set_important - Dataframe containing important cardinalities for the different sets

    df_card_set_important = pd.read_csv(src_dir + "card_set_important.csv")
    return df_card_set_important


def _reac_metabolic_network_sections_from_csv(src_dir):
    # Function for reading the metabolic network sections from a csv file
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # df_sections - Dataframe containing sections for metabolic network

    df_sections = pd.read_csv(src_dir + "sections.csv")
    return df_sections


def _convert_panda_to_numpy(df):
    ## Function that converts a pandas dataframe produced during preprocessing into a numpy array and removes the additions stemming from the conversion
    # Inputs:
    # df - pandas dataframe containing the 2D-array
    #
    # Output:
    # arr - numpy array containing all necessary entries

    # Convert pandas dataframe to numpy object
    arr = df.to_numpy()
    # Slice off the first column, as this is the enumerator from pandas
    arr = arr[:, 1:]
    return arr


def _recreate_list_of_sets(df):
    ## Function that recreates the list containing multiple sets, used to specify the nonzero entries of matrices
    # Inputs:
    # df - pandas dataframe that contains the list of sets, where each row is a new entry in the list, and each column in a given row are the members of the set
    #
    # Outputs:
    # lst - list containing the sets

    # Treat list as 2D numpy array
    arr = _convert_panda_to_numpy(df)
    # Move through 2D array row by row to create list of numpy arrays
    lst = []
    for i in range(arr.shape[0]):
        arr_no_nan = arr[i, ~np.isnan(arr[i, :])]
        # Change data type to int as we'll later use as indices
        arr_no_nan = arr_no_nan.astype(int)
        lst.append(arr_no_nan)
    return lst


def preprocessing_metabolic_network_matrix_parameters(src_dir):
    # Function for reading the metabolic network parameters that are in matrix form from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # S - stoichiometric matrix S
    # B - mapping matrix B
    # Q - additional constraint matrix Q
    # S_trans -  stoichiometric matrix transpose S_trans
    # Q_trans -  additional constraint matrix transpose Q_trans

    # Read metabolic network parameters from csv files and return pandas dataframes containing the matrices
    # Input: string to csv file location; Output: pandas dataframes for all matrices
    [DF_S, DF_B, DF_Q, DF_S_trans, DF_Q_trans] = _read_metabolic_network_matrix_parameters_from_csv(src_dir)

    # Converts pandas datamrames into 2D numpy arrays which are used in the optimization problem
    S = _convert_panda_to_numpy(DF_S)
    B = _convert_panda_to_numpy(DF_B)
    Q = _convert_panda_to_numpy(DF_Q)
    S_trans = _convert_panda_to_numpy(DF_S_trans)
    Q_trans = _convert_panda_to_numpy(DF_Q_trans)

    return S, B, Q, S_trans, Q_trans


def preprocessing_metabolic_network_vector_parameters(src_dir):
    # Function for reading the metabolic network parameters that are in vector form from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # b - right hand side of the equation S*v=b
    # c_bio - vector with a 1 at the index of the biomass reaction
    # c_obj - vector with ones at the index of reactions contributing to the lnetwork-level objective
    # c_glc - vector with a 1 at the index of the glucose uptake reaction
    # ub - upper bounds of the reactions
    # lb - lower bounds of the reactions
    # d - right hand side of the equation Q*v<=d

    # Read metabolic network parameters concerning vectors from csv files and return pandas dataframes containing the vectors
    # Input: string to csv file location; Output: pandas dataframes for all vectors
    [DF_b, DF_c_bio, DF_c_obj, DF_c_glc, DF_ub, DF_lb, DF_d] = _read_metabolic_network_vector_parameters_from_csv(src_dir)

    # Converts pandas dataframes into 1D numpy arrays which are used in the optimization problem
    # Additional flatten to get 1D shape
    b = _convert_panda_to_numpy(DF_b).flatten()
    c_bio = _convert_panda_to_numpy(DF_c_bio).flatten()
    c_obj = _convert_panda_to_numpy(DF_c_obj).flatten()
    c_glc = _convert_panda_to_numpy(DF_c_glc).flatten()
    ub = _convert_panda_to_numpy(DF_ub).flatten()
    lb = _convert_panda_to_numpy(DF_lb).flatten()
    d = _convert_panda_to_numpy(DF_d).flatten()
    return b, c_bio, c_obj, c_glc, ub, lb, d


def preprocessing_metabolic_network_set_parameters(src_dir):
    # Function for reading the metabolic network parameters that are sets concerning nonzero entries from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # set_nonzero_S - set of all nonzero entries in stoichiometric matrix S
    # set_nonzero_S_trans - set of all nonzero entries in stoichiometric matrix transpose S_trans
    # set_nonzero_Q - set of all nonzero entries in additional constraint matrix Q
    # set_nonzero_B - set of all nonzero entries in mapping matrix B


    # Read metabolic network parameters concerning sets from csv files and return pandas dataframes containing the sets
    # Input: string to csv file location; Output: pandas dataframes for all sets
    [DF_set_nonzero_S, DF_set_nonzero_S_trans, DF_set_nonzero_Q, DF_set_nonzero_B] = _read_metabolic_network_set_parameters_from_csv(src_dir)


    # Converts pandas dataframes into list of 1D numpy arrays which are used in the optimization problem
    row_list_of_nonzero_columns_S = _recreate_list_of_sets(DF_set_nonzero_S)
    row_list_of_nonzero_columns_S_trans = _recreate_list_of_sets(DF_set_nonzero_S_trans)
    row_list_of_nonzero_columns_Q = _recreate_list_of_sets(DF_set_nonzero_Q)
    row_list_of_nonzero_columns_B = _recreate_list_of_sets(DF_set_nonzero_B)

    return row_list_of_nonzero_columns_S, row_list_of_nonzero_columns_S_trans, row_list_of_nonzero_columns_Q, row_list_of_nonzero_columns_B


def preprocessing_knockout_set_parameters(src_dir):
    # Function for reading the metabolic network parameters that are sets concerning possible knockouts from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # set_reactions_not_to_target - set of all reactions that are definitely present in solution (y=1)
    # set_blocked_reactions - set of all reactions that cannot be reached and are thus fixed to zero(y=0)
    # set_possible_knockouts - set of all reactions that can be knocked out

    # Read metabolic network parameters concerning sets from csv files and return pandas dataframes containing the sets
    # Input: string to csv file location; Output: pandas dataframes for all sets
    [DF_set_reactions_not_to_target, DF_set_blocked_reactions, DF_set_possible_knockouts] = _read_knockout_set_parameters_from_csv(src_dir)


    # Converts pandas dataframes into 1D numpy arrays which are used in the optimization problem
    # Flatten due to being originally  1D sets
    # Ensure that all entries are integer
    set_reactions_not_to_target = _convert_panda_to_numpy(DF_set_reactions_not_to_target).flatten()
    set_reactions_not_to_target = set_reactions_not_to_target.astype(int)
    set_blocked_reactions = _convert_panda_to_numpy(DF_set_blocked_reactions).flatten()
    set_blocked_reactions = set_blocked_reactions.astype(int)
    set_possible_knockouts = _convert_panda_to_numpy(DF_set_possible_knockouts).flatten()
    set_possible_knockouts = set_possible_knockouts.astype(int)

    return set_reactions_not_to_target, set_blocked_reactions, set_possible_knockouts


def preprocessing_metabolic_network_indices_parameters(src_dir,target_chem,substrate_name):
    # Function for reading the metabolic network parameters that are indices or cardinalities of sets from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    # target_chem - id of target chemical to find correct index
    # substrate_name - id of substrate to find correct index
    # Outputs:
    # idx_important - indices of biomass reaction, the target chemical and the glucose and oxygen reaction TODO: Change preprocessing to more easily switch between target chemicals by importing all target chemicals(?)

    # Read metabolic network parameters for important indices from csv files and return pandas dataframe containing the indices
    # Input: string to csv file location; Output: pandas dataframes containing the indices
    [DF_idx_important, DF_ex_chem] = _read_metabolic_network_index_parameters_from_csv(src_dir)

    # Converts pandas dataframes into 1D numpy arrays
    # Flatten due to being originally  1D sets
    # Ensure that all entries are integer
    idx_important = _convert_panda_to_numpy(DF_idx_important).flatten()
    idx_important = idx_important.astype(int)
    # Uncluster the important indices
    idx_bm_forward = idx_important[0]
    idx_glc_backwards = idx_important[1] #Default of glucose as substrate
    idx_o2_backwards = idx_important[2]


    # Get index of target chemical
    # First get list of all possible target chemicals from dataframe for clear error handling
    ex_react = list(DF_ex_chem.index.values)
    # Check that input target_chemi is in possible options, necessary as pandas error is not immediately clear
    assert target_chem in ex_react, f"Could not find '{target_chem}' as possible secretion reaction, list of possible target chemicals contains {ex_react}"

    idx_target_chem_forward = DF_ex_chem.loc[target_chem, 'Reaction index']
    idx_substrate_backwards = DF_ex_chem.loc[substrate_name, 'Reaction index']
    # Convert to int
    idx_target_chem_forward = int(idx_target_chem_forward)
    idx_substrate_backwards = int(idx_substrate_backwards)
    return idx_bm_forward, idx_glc_backwards, idx_substrate_backwards, idx_o2_backwards, idx_target_chem_forward


def preprocessing_metabolic_cardinality_parameters(src_dir):
    # Function for reading the metabolic network parameters that are indices or cardinalities of sets from the corresponding csv files
    # Input:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # card_set_important - important cardinalities for the different sets

    # Read metabolic network parameters for set cardinalities from csv files and return pandas dataframes containing the sets
    # Input: string to csv file location; Output: pandas dataframes containing the set cardinalities
    DF_card_set_important = _read_metabolic_network_cardinality_parameters_from_csv(src_dir)

    # Converts pandas dataframes into 1D numpy arrays
    # Flatten due to being originally  1D sets
    # Ensure that all entries are integer
    card_set_important = _convert_panda_to_numpy(DF_card_set_important).flatten()
    card_set_important = card_set_important.astype(int)
    # Uncluster the maximum ranges of different sets
    num_metabolites = card_set_important[0]
    num_reactions_ori = card_set_important[1]
    num_reactions_irrev = card_set_important[2]
    num_add_constr = card_set_important[3]
    return num_metabolites, num_reactions_ori, num_reactions_irrev, num_add_constr

def preprocessing_sections(src_dir):
    # Function for reading the sections inside the metabolic network
    # Inputs:
    # src_dir - string containing the folder where the csv files are
    #
    # Outputs:
    # sections - sections of metabolic network containing subsequent reactions only
    DF_sections = _reac_metabolic_network_sections_from_csv(src_dir)

    # Convert pandas dataframe to 2D- list of lists
    sections = _convert_panda_to_numpy(DF_sections)

    sections = sections.tolist()
    for i,section in enumerate(sections):
        section = [int(item) for item in section if not np.isnan(item)]
        sections[i] = section

    return sections


def print_metabolic_network_information(src_dir):
    # Function for printing out the name of the metabolic network that is in use and the target chemical that is set by default
    # Inputs:
    # src_dir - string containing the folder where the csv files are
    # print out network name and target chemical
    file = open(src_dir + "network_information.txt")
    content = file.read()
    print(content)
    file.close()

